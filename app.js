//  app new
// app done 1
// app delete 1
// app list
// app.js
const commands= require('./taskManager').commands;
const TaskManager=require('./taskManager').TaskManager;
const chalk=require('chalk');
const prompt=require('prompt');

let command=process.argv[2];
if(commands.indexOf(command)==-1){
    console.log(chalk.red('command is not Valid!'))
}

let taskManagerApp=new TaskManager;
 console.log(taskManagerApp)
  switch (command){
      case 'new':
         taskManagerApp.createNewTask();
         break;
      case 'list':
          taskManagerApp.printTasks();
          break;
      case 'delete':
          let TheIndex=process.argv[3];
          taskManagerApp.deleteTask(TheIndex);
          break;
      case 'done':
          let taskIndex=process.argv[3];
          taskManagerApp.setDone(taskIndex);
          break;
  }
// let myTry=commands.map(function(value){return value.toLowerCase()});
// console.log(myTry);