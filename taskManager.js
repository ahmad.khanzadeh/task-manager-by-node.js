//  app new
// app done 1
// app delete 1
// app list
// TaskManager.js
const commands=['new','done','delete','list'];
const { timeStamp } = require('console');
const fs= require('fs');
const tasksFileName='tasks.json';
const prompt= require('prompt');
const chalk=require('chalk');
 class TaskManager{
    tasks=[];
    constructor(){
        this.init();
        //  this.createNewTask();
    }
    init(){
       let tasksFile= this.getOrCreateTasksFile();
       this.tasks = JSON.parse(tasksFile);
    }
    getOrCreateTasksFile(){
        if(! fs.existsSync(tasksFileName)){
            fs.writeFileSync(tasksFileName,JSON.stringify([]));
        }
        return fs.readFileSync(tasksFileName,"utf-8")
    }
    printTasks(){
        this.tasks.map((task,index)=>{
            if(task.done){
                console.log(chalk.green(index +' '+task.title));
            }else{
                console.log(chalk.yellow(index + ' ' +task.title));
            }
        })
    }
    deleteTask(taskIndex){
        this.tasks.splice(taskIndex,1);
        this.updateTasksFile()
    }
    setDone(taskIndex){
        this.tasks[taskIndex].done=true;
        let timeEnded=new Date().getTime();
        // let  taskDuration=timeEnded-this.tasks[taskIndex].timeStamp;
        // console.log("Duration of Task:");
        // console.log(taskDuration)
        this.updateTasksFile();
        this.printTasks();
    }
    createNewTask(){
        console.log("تا اینجا کار کرد")
        prompt.start();
        prompt.get(['task'],(err, result)=>{
             let task={
                title: result.task,
                timestamp: new Date().getTime(),
                // وضعیت اولیه
                 done: false    
               }
               this.tasks.push(task);
               this.updateTasksFile();
            });
    }
    updateTasksFile(){
        fs.writeFile(tasksFileName,JSON.stringify(this.tasks),function(err){if(!err){console.log("File Updated")}})
    }
 }
 module.exports={
     commands:commands,
     TaskManager:TaskManager
 }